package com.example.restfulapi.response;

import java.util.HashMap;
import java.util.Map;

public class APIRequest {

    private static String Endpoint = "https://rickandmorty.com/api";
    private String path;
    private String method;
    private Map<String, String> headers;
    private Map<String, Object> parameters;

    public APIRequest(String method, String path, Map<String, String> headers){
        this.path = path;
        this.method = method;
        this.headers = headers;
        this.parameters = new HashMap<String, Object>();
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }



}
