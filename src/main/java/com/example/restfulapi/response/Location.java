package com.example.restfulapi.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import java.util.List;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)

public class Location{

    private String id;
    private String name;
    private String type;
    private List<String> residents;
    private String url;


}