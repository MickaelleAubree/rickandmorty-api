package com.example.restfulapi.controller;

import com.example.restfulapi.client.RickAndMortyClient;
import com.example.restfulapi.response.Episode;
import com.example.restfulapi.response.Location;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/webclient")
@AllArgsConstructor
public class RickAndMortyController {

    RickAndMortyClient rickAndMortyClient;


    @GetMapping("/character/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Character> getCharacterById(@PathVariable String id) {
        return rickAndMortyClient.findACharacterById(id);

    }


    @GetMapping("/location/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Location> getLocationById(@PathVariable String id) {
        return rickAndMortyClient.findALocationById(id);

    }


    @GetMapping("/episode/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Episode> getEpisodeById(@PathVariable String id) {
        return rickAndMortyClient.findAEpisodeById(id);

    }


    @GetMapping("/episodes")
    @ResponseStatus(HttpStatus.OK)
    public Flux ListAllEpisodes() {
        return rickAndMortyClient.ListAllEpisodes();

    }


}
