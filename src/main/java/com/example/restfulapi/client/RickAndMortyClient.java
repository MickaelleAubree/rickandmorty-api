package com.example.restfulapi.client;

import com.example.restfulapi.response.Episode;
import com.example.restfulapi.response.ListOfEpisodes;
import com.example.restfulapi.response.Location;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
public class RickAndMortyClient<WebClient> {

    private final WebClient webClient;

    public RickAndMortyClient(WebClient builder) {

        webClient = builder.baseUrl("https://rickandmortyapi.com/api").build();
    }

    public Mono<Character> findACharacterById(String id) {
        log.info("Character with id [{}]", id);
        return webClient
                .get()
                .uri("/character/" + id)
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Character.class);
    }

    public Mono<Location> findALocationById(String id) {
        log.info("Find location with id [{}]", id);
        return webClient
                .get()
                .uri("/location/" + id)
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Location.class);
    }


    public Mono<Episode> findAEpisodeById(String id) {
        log.info("Fin episode with id [{}]", id);
        return webClient
                .get()
                .uri("/episode/" + id)
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Episode.class);
    }

    public Flux ListAllEpisodes() {
        log.info("All episodes");
        return webClient
                .get()
                .uri("/episode")
                .accept(APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(ListOfEpisodes.class);
    }

}

